package master2.ila.mmm.tp1;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CustomerDAO {

    @Query("SELECT * FROM customer_table")
    LiveData<List<Customer>> getAll();


    @Insert
    void insert(Customer customer);
}
