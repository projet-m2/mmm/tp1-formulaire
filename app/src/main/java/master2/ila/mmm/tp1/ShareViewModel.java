package master2.ila.mmm.tp1;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ShareViewModel extends AndroidViewModel {
    private CustomerRepository customerRepository;
    private LiveData<List<Customer>> allCustomers;

    public ShareViewModel(@NonNull Application application) {
        super(application);
        customerRepository = new CustomerRepository(application);
        allCustomers = customerRepository.getAllCustomers();
    }

    public void insert(Customer customer) {
        customerRepository.insert(customer);
    }
    
    public LiveData<List<Customer>> getAllCustomers() {
        return allCustomers;
    }
}
