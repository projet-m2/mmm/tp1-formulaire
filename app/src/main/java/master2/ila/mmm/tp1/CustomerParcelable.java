package master2.ila.mmm.tp1;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomerParcelable implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CustomerParcelable createFromParcel(Parcel in) {
            return new CustomerParcelable(in);
        }

        public CustomerParcelable[] newArray(int size) {
            return new CustomerParcelable[size];
        }
    };

    private String name;
    private String surname;
    private String birth;
    private String city;
    private String departments;
    private String phone;


    public CustomerParcelable(String name, String surname, String birth, String city, String departments) {
        this.name=name;
        this.surname=surname;
        this.birth=birth;
        this.city=city;
        this.departments=departments;
    }

    public CustomerParcelable(String name, String surname, String birth, String city, String departments, String phone) {
        this.name=name;
        this.surname=surname;
        this.birth=birth;
        this.city=city;
        this.departments=departments;
        this.phone=phone;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirth() {
        return birth;
    }

    public String getCity() {
        return city;
    }

    public String getDepartments() {
        return departments;
    }

    public String getPhone() {
        return phone;
    }

    public CustomerParcelable(Parcel in) {
        this.name=in.readString();
        this.surname=in.readString();
        this.birth=in.readString();
        this.city=in.readString();
        this.departments=in.readString();
        this.phone=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(birth);
        dest.writeString(city);
        dest.writeString(departments);
        dest.writeString(phone);
    }


}
