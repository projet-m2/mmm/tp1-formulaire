package master2.ila.mmm.tp1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {
    private List<Customer> customersList = new ArrayList<>();
    private CustomerAdapterListener listener;

    public CustomerAdapter(CustomerAdapterListener listener){
        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, surname;
        public ImageView imageView;

        public MyViewHolder(View view){
            super(view);
            name = view.findViewById(R.id.item_name);
            surname = view.findViewById(R.id.item_surname);
            imageView = view.findViewById(R.id.imageView2);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onCustomerSelected(customersList.get(getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Customer customer = customersList.get(position);
        holder.name.setText(customer.getName());
        holder.surname.setText(customer.getSurname());
        if(customer.getByteArray() != null){
            Bitmap bmp = BitmapFactory.decodeByteArray(customer.getByteArray(), 0, customer.getByteArray().length);
            holder.imageView.setImageBitmap(bmp);
        }
    }

    @Override
    public int getItemCount() {
        return customersList.size();
    }


    public interface CustomerAdapterListener {
        void onCustomerSelected(Customer customer);
    }

    public void setCustomersList(List<Customer> customersList) {
        this.customersList = customersList;
        notifyDataSetChanged();
    }

}
