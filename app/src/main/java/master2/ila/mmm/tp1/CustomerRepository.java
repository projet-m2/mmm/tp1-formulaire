package master2.ila.mmm.tp1;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CustomerRepository {

    private CustomerDAO customerDAO;
    private LiveData<List<Customer>> allCustomers;

    public CustomerRepository(Application application){
        AppDataBase appDataBase = AppDataBase.getInstance(application);
        customerDAO = appDataBase.customerDAO();
        allCustomers = customerDAO.getAll();
    }

    public void insert(Customer customer){
        new InsertCustomerAsyncTask(customerDAO).execute(customer);
    }

    public LiveData<List<Customer>> getAllCustomers(){
        return allCustomers;
    }

    private static class InsertCustomerAsyncTask extends AsyncTask<Customer, Void, Void> {
        private CustomerDAO customerDAO;

        private InsertCustomerAsyncTask(CustomerDAO customerDAO) {
            this.customerDAO = customerDAO;
        }

        @Override
        protected Void doInBackground(Customer... customers) {
            customerDAO.insert(customers[0]);
            return null;
        }
    }
}
