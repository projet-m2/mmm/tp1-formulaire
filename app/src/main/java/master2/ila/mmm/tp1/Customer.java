package master2.ila.mmm.tp1;


import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "customer_table")
public class Customer {

    @PrimaryKey(autoGenerate = true)
    public int id;

    String name, surname, birthday, city, department, phone;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    byte[] byteArray;

    public Customer(String name, String surname, String birthday, String city, String department) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.city = city;
        this.department = department;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getCity() {
        return city;
    }

    public String getDepartment() {
        return department;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone){
        this.phone=phone;
    }

    public byte[] getByteArray(){
        return this.byteArray;
    }

    public void setByteArray(byte[] byteArray){
        this.byteArray = byteArray;
    }

}
