package master2.ila.mmm.tp1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityIntent extends AppCompatActivity {
    @BindView(R.id.display_name) TextView name;
    @BindView(R.id.display_surname) TextView surname;
    @BindView(R.id.display_birthday) TextView birthday;
    @BindView(R.id.display_city) TextView city;
    @BindView(R.id.display_department) TextView department;
    @BindView(R.id.display_phone) TextView phone;
    @BindView(R.id.imageView3) ImageView profilePicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);


        Intent intent = getIntent();
        if (intent != null){
            CustomerParcelable customerParcelable = intent.getParcelableExtra("PARCELABLE");
            if (customerParcelable != null){
                if(customerParcelable.getName()!= null){
                    name.setVisibility(View.VISIBLE);
                    name.append(customerParcelable.getName());
                }
                if(customerParcelable.getSurname()!= null){
                    surname.setVisibility(View.VISIBLE);
                    surname.append(customerParcelable.getSurname());
                }
                if(customerParcelable.getBirth()!= null){
                    birthday.setVisibility(View.VISIBLE);
                    birthday.append(customerParcelable.getBirth());
                }
                if(customerParcelable.getCity()!= null){
                    city.setVisibility(View.VISIBLE);
                    city.append(customerParcelable.getCity());
                }
                if(customerParcelable.getDepartments()!= null){
                    department.setVisibility(View.VISIBLE);
                    department.append(customerParcelable.getDepartments());
                }
                if(customerParcelable.getPhone() != null){
                    phone.setVisibility(View.VISIBLE);
                    phone.append(customerParcelable.getPhone());
                }
            }

            byte[] byteArray = intent.getExtras().getByteArray("PP");
            if(byteArray != null){
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                profilePicture.setImageBitmap(bmp);
            }
        }
    }

}
