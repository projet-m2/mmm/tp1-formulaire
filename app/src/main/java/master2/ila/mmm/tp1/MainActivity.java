package master2.ila.mmm.tp1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.name) EditText name;
    @BindView(R.id.surname) EditText surname;
    @BindView(R.id.birth) Button birthday;
    @BindView(R.id.city) EditText city;
    @BindView(R.id.departments) Spinner departments;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.wikipedia) Button wikipedia;
    @BindView(R.id.camera) Button camera;
    @BindView(R.id.imageView) ImageView imageView;


    private String dateString = "";
    private CustomerParcelable customer;

    private byte[] byteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(camera_intent, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar customer_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.clear:
                name.getText().clear();
                surname.getText().clear();
                birthday.setText("Birthday");
                city.getText().clear();
                phone.getText().clear();
                dateString = "";
                return true;
            case R.id.add:
                phone.setVisibility(View.VISIBLE);
                return true;
            case R.id.photo:
                camera.setVisibility(View.VISIBLE);
                default:return super.onOptionsItemSelected(item);
        }




    }

    public void valider(View view) {
        String nameText = name.getText().toString();
        String surnameText = surname.getText().toString();
        String cityText = city.getText().toString();
        String departmentText = departments.getSelectedItem().toString();
        String phoneText = phone.getText().toString();

        String toastString = "Name : "+ nameText
                + "\nSurname :  "+ surnameText
                + "\nBirthday :  "+ dateString
                + "\nCity : " + cityText
                + "\nDepartment : " + departmentText;

        Log.i("valider","test");

        if(phone.getVisibility() == View.VISIBLE) {
            toastString += "\nPhone Number : " + phoneText;
            customer = new CustomerParcelable(nameText, surnameText, dateString, cityText, departmentText, phoneText);
        }
        else{
            customer = new CustomerParcelable(nameText, surnameText, dateString, cityText, departmentText);
            if(camera.getVisibility() == View.VISIBLE){

            }
        }

        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_SHORT).show();

        Intent sendToRecycler = new Intent(this, ActivityRecyclerView.class);
        sendToRecycler.putExtra("CUSTOMER", customer);
        sendToRecycler.putExtra("PP", byteArray);
        setResult(RESULT_OK, sendToRecycler);
        finish();
    }

    public void datePicker(View view){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                dateString = dateFormat.format(newDate.getTime());
                birthday.setText("Born the : "+dateString);
            }
        },year,month,dayOfMonth);
        datePickerDialog.show();
    }

    public void invokeWikipedia(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.wikipedia.org/wiki/"+parseDepartment(departments.getSelectedItem().toString())+"_(département)"));
        startActivity(intent);
    }

    private String parseDepartment(String department){
        return department.split(" - ")[1];
    }

    public void takePicture(View view){

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(image);
            imageView.setVisibility(View.VISIBLE);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
        }
    }

}

