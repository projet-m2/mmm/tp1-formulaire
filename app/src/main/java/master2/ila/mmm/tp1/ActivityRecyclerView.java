package master2.ila.mmm.tp1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityRecyclerView extends AppCompatActivity implements CustomerAdapter.CustomerAdapterListener{

    @BindView(R.id.recycler) RecyclerView recyclerView;

    private Customer customer;
    private ShareViewModel shareViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Recycler View");

        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerView.getContext(), 1);
        recyclerView.addItemDecoration(itemDecor);

        // on créée une instance d'adapter (qui contient une copie de la donnée,
        // et la déclaration des ViewHolder
        final CustomerAdapter customerAdapter = new CustomerAdapter(this);
        recyclerView.setAdapter(customerAdapter);

        // on crée une instance de notre ViewModel
        shareViewModel = ViewModelProviders.of(this).get(ShareViewModel.class);

        // et on ajoute un observer sur les utilisateurs...
        shareViewModel.getAllCustomers().observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(@Nullable List<Customer> customers) {
                customerAdapter.setCustomersList(customers);
            }
        });
    }

        public void addCustomer(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onCustomerSelected(Customer customer){
        Intent sendToIntent = new Intent(this, ActivityIntent.class);
        sendToIntent.setAction(Intent.ACTION_SEND);
        sendToIntent.setType("text/plain");
        sendToIntent.putExtra("PARCELABLE", new CustomerParcelable(customer.getName(),
                customer.getSurname(),
                customer.getBirthday(),
                customer.getCity(),
                customer.getDepartment(),
                customer.getPhone()));
        if(customer.getByteArray() != null){
            sendToIntent.putExtra("PP", customer.getByteArray());
        }
        // Start the activity
        startActivity(sendToIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK && requestCode == 1){
            CustomerParcelable result = data.getParcelableExtra("CUSTOMER");
            customer = new Customer(result.getName(), result.getSurname(), result.getBirth(), result.getCity(), result.getDepartments());
            if(result.getPhone() != null){
                customer.setPhone(result.getPhone());
            }
            shareViewModel.insert(customer);

            byte[] byteArray= data.getExtras().getByteArray("PP");
            customer.setByteArray(byteArray);
        }
    }
}
