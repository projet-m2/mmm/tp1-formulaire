package master2.ila.mmm.tp1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.ByteArrayOutputStream;
import java.io.File;

@Database(entities = {Customer.class}, version = 2, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {

    private static AppDataBase instance;

    public abstract CustomerDAO customerDAO();

    public static synchronized AppDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class, "note_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }


    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private CustomerDAO noteDao;

        private PopulateDbAsyncTask(AppDataBase db) {
            noteDao = db.customerDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Customer customer1 = new Customer("Windrunner","Sylvanas", "01/01/1994", "World Of Warcraft", "Blizzard");
            Customer customer2 = new Customer("Stormrage","Illidan", "01/01/1994", "World Of Warcraft", "Blizzard");
            Customer customer3 = new Customer("King","Lich", "01/01/1994", "World Of Warcraft", "Blizzard");
            customer3.setPhone("0606060606");

            noteDao.insert(customer1);
            noteDao.insert(customer2);
            noteDao.insert(customer3);

            return null;
        }
    }

}
